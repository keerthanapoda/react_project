import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';

@Component({
  selector: 'app-showdetails',
  templateUrl: './showdetails.component.html',
  styleUrls: ['./showdetails.component.css']
})
export class ShowdetailsComponent implements OnInit {
  farmer:any;
  constructor(private httpClient: HttpClient,private service:UserService) { }

  ngOnInit(): void {
    this.service.fetchDetails().subscribe((result:any)=>{
      this.farmer = result;
      
    });
  
  }


}