
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { UserService } from '../user.service';
//import {UserService} from '../user.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  farmer:any;

  constructor(private httpClient:HttpClient,private service:UserService) { 
    this.farmer ={name:'', email:'',password:'',mobile:''};
  }

  ngOnInit(): void {
  }
  irrigreat(){
    this.service.irrigreat(this.farmer).subscribe(
      (result:any)=>console.log(result));
    
  }

}